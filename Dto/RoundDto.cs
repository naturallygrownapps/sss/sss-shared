﻿using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Shared
{
    /// <summary>
    /// Represents one round of a game, i.e. a single turn in which each player provides a <see cref="Move"/> (rock, paper or scissors).
    /// </summary>
    public class RoundDto
    {
        private Move? _yourMove;
        public Move? YourMove
        {
            get { return _yourMove; }
            set { _yourMove = value; }
        }

        private Move? _otherPlayerMove;
        public Move? OtherPlayerMove
        {
            get { return _otherPlayerMove; }
            set { _otherPlayerMove = value; }
        }

        public RoundDto()
        {
        }
    }
}