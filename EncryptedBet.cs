﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Shared
{
    public class EncryptedBet
    {
        public byte[] Data { get; set; }
        public byte[] SourceKeyAndIV { get; set; }
        public byte[] ReceiverKeyAndIV { get; set; }
    }
}
