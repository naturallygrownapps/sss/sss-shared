﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Shared
{
    public enum Move
    {
        Rock, Paper, Scissors
    }
}