﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Shared
{
    public enum BetCapabilities: int
    {
        Photo,
        Location,
        TextMessage
    }
}