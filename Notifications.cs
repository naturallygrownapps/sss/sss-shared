﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Shared
{
    public enum NotificationPlatform
    {
        Android
    }

    public enum NotificationType
    {
        NewGame, GameUpdated, GameRejected
    }

    public class Notification
    {
        public NotificationType Type { get; set; }

        public Notification(NotificationType type)
        {
            Type = type;
        }
    }

    public class NewGameStartedNotification: Notification
    {
        public Guid GameId { get; set; }

        public NewGameStartedNotification() 
            : base(NotificationType.NewGame)
        {
        }
    }

    public class GameUpdatedNotification : Notification
    {
        public Guid GameId { get; set; }

        public GameUpdatedNotification() 
            : base(NotificationType.GameUpdated)
        {
        }
    }

    public class GameRejectedNotification: Notification
    {
        public Guid GameId { get; set; }

        public GameRejectedNotification() 
            : base(NotificationType.GameRejected)
        {
        }
    }

}
