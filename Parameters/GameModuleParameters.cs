﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Shared.Parameters
{
    public class GameModuleParameters
    {
        public class ChallengeInput
        {
            public string ChallengedUsername { get; set; }
            public byte[] PublicKey { get; set; }
            public BetCapabilities[] BetTypeCaps { get; set; }
        }

        public class MoveInput
        {
            public Guid GameId { get; set; }
            public Move Move { get; set; }
        }

        public class OpponentAcceptInput
        {
            public Guid GameId { get; set; }
            public byte[] PublicKey { get; set; }
            public BetCapabilities BetType { get; set; }
            public EncryptedBet Bet { get; set; }
        }

        public class ChallengerBetInput
        {
            public Guid GameId { get; set; }
            public EncryptedBet Bet { get; set; }
        }

        public class OpponentRejectInput
        {
            public Guid GameId { get; set; }
        }

        public class AcceptRejectionInput
        {
            public Guid GameId { get; set; }
        }

        public class GetBetInput
        {
            public Guid GameId { get; set; }
        }

        public class QueryInput
        {
            public Guid GameId { get; set; }
        }

        public class MyGamesInput
        {
            public byte[] GameKey { get; set; }
        }
    }
}